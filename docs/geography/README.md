# Geography of the Tung Jung Districts

__Tung__ refers to Tung Gwoon ([Dongguan](https://en.wikipedia.org/wiki/Dongguan))

__Jung__ refers to Jungsen ([Zengcheng](https://en.wikipedia.org/wiki/Zengcheng_District))

[Hanyu Pinyin](https://en.wikipedia.org/wiki/Pinyin) is used below to reference place names.

## Dongguan - A Survey of Dongguan City

Dongguan City is located at the lower reaches of the Dongjiang River and the northeastern part of the Pearl River Delta. The city borders Boluo and Huizhou on the east, connects with Shenzhen on the south, Panyu on the west and the middle of Guangzhou - Hong Kong Economic Corridor. The city possesses a total of 2,465 square kilometres, cultivated land of 80,000 hectares and a population of 1,310,000. There are 180,000 Dongguan natives residing in foreign countries and 650,000 in Hong Kong and Macao.

![map of Dongguan](images/map.gif)


The County of Dongguan was founded during the Wu State of Three Kingdoms. Dongguan is of glorious tradition of revolution. National hero Lin Zexu destroyed opium at the county, that adds an illustrious chapter to the annals of the modern history of China. During the War of Resistance Against Japan and China's War of Liberation, Dongguan was the revolutionary base of the Communist Party's Dongjiang Column.

In 1985, the State Council ratified Dongguan as an economic open zone of the Pearl River Delta and Dongguan City was founded instead of the County title. Beginning from 1988, Dongguan has been upgraded into a prefecture administering 33 towns and districts and setting up 583 administration zones. Dongguan City proper is located at Guancheng Town.

In 1990, Dongguan City's total output value has been estimated to reach RMB 11.47 billion, GNP reached RMB 5.78 billion, the gross output value of industry and agriculture of the city was RMB 7.12 billion and the national income of the city was RMB 4.83 billion, that were the increases of 970%, 850%, 810% and 770% over 1978. 1990, there are increases over 1978 as financial turnover by 273%, tax revenue by 600%, origination of currencies by 1001% the income per person in the countryside increased from RMB 193 to RMB 1,350. The income per person in the city increased from RMB 474 to RMB 3,510, last year, the deposit in the countryside and city reached RMB 4.54 billion. The income per person reached RMB 4.54 billion. The income per person reached RMB 3388 and most of the people attained the well-to-do level. People notice the thriving undertakings in culture and education, science and technology, public health and sports; people live and work in peace and contentment.

![Zeng Cheng Map](images/zhengchengmap.gif)

[General]--Zengcheng Municipality, with 900,000 permanent residents, and a population of over 270,000 living in Hong Kong, Macao, Taiwan and overseas originating from Zengcheng, covering an area of 1,741 square kilometers, being one of Guangzhou satellite cities, is located in the northeastern part of the Pearl River Delta. Zengcheng, with elegancy environment, is affluent in natural resources. Communication and traffic are consummate. Zengcheng is enjoying a reputation of "Golden Corridor" which links with several big cities at the Pearl River Delta.

## Regional Map of Zengcheng Municipality

![Regional Zengcheng Map](images/zhengchengreg.gif)
