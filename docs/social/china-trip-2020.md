# Tung Jung Association Trip to Guangzhou and Xintang 2020

This was scheduled for March 2020 but in view of the [Covid-19](https://en.wikipedia.org/wiki/2019%E2%80%9320_Wuhan_coronavirus_outbreak) virus and pandemic, it has now been postponed until further notice.

Please contact [Gordon Wu](/contacts/#gordon-wu) for more details.
