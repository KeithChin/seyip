# Contact Details

## The Seyip Association of NZ Inc

```
The Seyip Association of NZ Inc
23 Ghuznee St
PO Box 9093
Wellington 6011
NEW ZEALAND
email: seyipnz@gmail.com
```

## Membership

The financial year for the Association begins on 1 April. To ensure your
membership remains current please complete the membership form and send it
to seyipnz@gmail.com or post to PO Box 9093 Wellington. 
Pease make membership payment via direct credit & ensure your name & contact number is completed in the ref fields
Please include your ancestral village. This will add to the records of the Association.



Click [here](/contact/membership-2020.pdf) for the Membership Form April 2020 - March 2021 

## President

```
Mr Keith Chin
NEW ZEALAND
Phone +64 4 XXXXX 
Mobile +64 022 
Email: 
```

## English Secretary

```
Quin wing
Mobile 
Email: 
```

## Chinese Secretary

```
Lawrence Kwang
Karori
Wellington
 
```

## Webmaster

```
Dr 
Email: 
```

## XXXX

```
Mr 
```

## Vice President

```
Chiao Tam
Email: 
```

## Treasurer

```
Colin Chin
Email: treasurer@
```

## Social Committee

``` 
XXXX
```
