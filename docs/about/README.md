# Welcome to The Seyip Association of New Zealand Inc

The Association was founded in XXXX by the Chinese who immigrated to New Zealand from the XXXXX and XXXXX hence Seyip, districts in the Guangdong province in Southern China.

This web site gives an overview of the history and the people who founded the Association and an insight into the work of the Association at the present time.

We hope that this web site will stimulate interest in the Association and in the history of the Chinese in New Zealand.

## Full Membership

All people whose ancestry are from the counties of XXXX and XXXX in Guangdong province in China are invited to apply for full membership to the Association.

## Associate Membership

People who do not qualify for full membership but wish to partake in the activities of the Seyip Association are welcome to apply for associate membership to the Association.  However, the constitution does not allow voting rights for associate members.  

Associate members may have ancestral homes outside of the Seyip area, and do not need to have any Chinese ancestry.  

:orange_book: [Membership form 2020](/contact/membership-2020.pdf) 

:orange_book: [Constitution Revised May 2001](assets/TungJung_Constitution.pdf)

 
