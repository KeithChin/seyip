---
home: true
heroImage: /underconstruction.png # https://www.tungjung.org.nz/images/tungjung-logo-large.png
heroText: SEYIP Association of NZ Inc (1949)
tagline: 新 西 蘭 四 邑 會 館
actionText: 開門 Enter →
actionLink: /about/
features:
- title: Cultural
  details: Promote the culture of the peoples from the four former counties of Xinhui (Sunwui), Taishan (Toisan), Kaiping (Hoiping) and Enping (Yanping) on the west side of the Pearl River Delta in Southern Guangdong Province, China.
- title: Education
  details: Teach the spoken Guangdong dialect of China, and Chinese literacy
- title: Social
  details: Organise social events for members and celebrate Chinese customs
footer: Copyright © 2023-present The Seyip Association of NZ Inc, Hosted on GitLab.com
---




