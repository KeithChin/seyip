# 2023 MEETINGS AND EVENTS

# Social

- New Year's Dinner Dragon's Restaurant Sunday 29th January, 2023 at 18:30
  contact Peter Moon for tickets $45 pp

  * New Year Banquet 2023
  
>MENU
>
>Soup
>
>Beef & Mushroom Soup
>
>Platter
>
>>1/2 Cantonese Roast Duck, 1/2 Crispy & Roast Pork
>
>
>Main
>
>Tender White Cut Chicken
>
>Sweet & Sour Pork 
>
>Steamed Blue Cod with Ginger, Spring & Onions & Sweet Soya Sauce 
>
>Stir Fried Beef & Broccoli
>
>
>Garlic Gai Lan
>
>
>Homemade Silken Tofu with Chicken Mince
>
>Wok Fried Vermicelli & Shrimps with Sauce
>
>Beverage
>
>Wine, Sparkling Grape Juice, Chinese Tea
>
>Dessert
>>
>>Sago with Coconut Cream
>>
>>Christmas New Year Cake
>>
>>Fresh Fruit Platter


# 2023 MEETINGS AND EVENTS

- AGM date on 13 August 2023 at 2pm in our rooms (Torrens Terrace) 

## Executive Committee Meetings

Executive Committee meetings usually start at 7:30 pm on the first Monday of each month, commencing 6th Feb 2023

# Social

- New Year's Dinner Dragon's Restaurant Sunday Jan 29, 2023

- Ching Ming Sunday 2nd April 2023 12:00 at Tung Jung Memorial Stele

- Mid-Winter Yum Cha @ Dragon's Restaurant 12 July 2023 12PM
  Contact [Social Committee](/contact/#social-committee) for tickets @ $25 pp (no cheques accepted)

- Moon Festival Sunday date in 1 October, 2023 18:00 Dragon's Restaurant $50 pp, RSVP to Peter Moon (20 tables max) 

- Chung Yeung Sunday date 22 October 2023 (9th day of the 9th month of the Lunar Calendar)

- Christmas Seniors' Yum Cha Wednesday 6th December 2023 Dragon's Restaurant $TBA pp
