# Chinese Language Classes (online/physical)

Cantonese Class restarts 2nd february, 2021  
Mandarin Class restarts 15th January, 2021 (no class on the 22nd January)

## Cantonese Classes

### Virtual

The video link will be sent out to students before the class starts.

- Intermediate Classes, 7:30 pm Tuesdays

- Beginner Classes, 7:30 pm Wednesdays (excepting 3rd Wednesdays of each month)

### Physical

- 7:30 - 9:30 pm Thursdays at Tung Jung Association rooms (parking next door)
 [Gordon Wu](/contact/#gordon-wu) to confirm attendance.

Please let the teacher [Gordon Wu](/contact/#gordon-wu) know if you are a new student wishing to join the classes.
There are currently no fees except for materials as necessary.  

## Mandarin Classes

Beginner's class is online on Fridays at 7:30 pm.  Contact [Gordon Wu](/contact/#gordon-wu) for the details

Note that the classes are recorded and the unlisted youtube link is sent to students the day or so after.
