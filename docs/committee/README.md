# The Executive Committee for 2023 - 2024
 

| Office         | Name                         |
|-------------------|--------------------------------------|
| President 會長        | Keith Chin 陳奇哺                      |
| Past president    |     Tony Tong                      |
| Vice President    |    Chiao Tam                      |
| Treasurer         | Colin Chin |
| English Secretary |   Quin Wing                    |
| Chinese Secretary |    Lawrence Kwang  |


## Video Conferencing

We have Google Hangouts MEET available to us which allows a group of up to 250 to meet.  This facility is provided free by Google until July, and hopefully extended if the crisis extends past that.

You need to login using your tungjung email address as that means as a part of the TungJung domain the person who starts the meeting doesn't have to let you into the meeting and it is sometimes not apparent that someone is waiting to be let in the room. So this is a manual process required for those logging in using their xtra, gmail or other identities.

Download the [Meet App for Android](https://play.google.com/store/apps/details?id=com.google.android.apps.meetings&hl=en) for Android users.  

And on iPhone [Meet App for Apple](https://apps.apple.com/us/app/hangouts-meet-by-google/id1013231476)

If you don't have a smart phone you can use a browser, and you'll receive a link to join the meeting.
